int ler(char *argumento[],int type)
{
    FILE *arq;
    int ascii,pos;
    char cpy[15];
    sprintf(cpy,"cripto%s",argumento[1]);
    arq = fopen(argumento[1],"rb");
    if(arq == NULL)
    {
        printf("Esse arquivo não existe\n");
        exit(1);
    }
    do
    {
        ascii = fgetc(arq);
        if(type==1)
        {
            cr(argumento[1],ascii);
        }
        else
        if(type==2)
        {
            dcr(argumento[1],ascii);
        }
        else
        {
            exit(0);
        }
    }
    while(ascii!=EOF);
    fclose(arq);
    remove(argumento[1]);
}