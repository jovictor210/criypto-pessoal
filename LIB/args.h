int arg(char *arg[])
{
    int type;
    if(arg[1]==NULL)
    {
        printf("\nCripto by Calisto\npara infos use './cripto help'\n");
        exit(0);
    }
    if(arg[1]==NULL&&arg[2]!=NULL)
    {
        printf("\nuse ./cripto help\n");
    }
    else
    if(arg[2]==NULL&&arg[1]!=NULL)
    {
        printf("\nuse ./cripto help\n");
    }
    else
    if((strcmp(arg[1],"help"))==0)
    {   
        printf("helper:\nExecute o programa usando\ncomo argumento o arquivo\nque quer usar seguido do modo\n\nModos: encr decr\nexemplos:\n./cripto arq.txt encr\n./cripto livro.txt decr\n");
        type = 3;
        exit(0);
    }
    else
    {    
        if((strcmp(arg[2],"encr"))==0)
        {
            type = 1;
        }
        else
        if((strcmp(arg[2],"decr"))==0)
        {
            type = 2;
        }
        else
        {
            printf("Parametro desconhecido, procure ajuda com ./cripto help\n");
            exit(0);
        }
    }
    return type;
}